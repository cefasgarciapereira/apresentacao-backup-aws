// Import React
import React from "react";

// Import Spectacle Core tags
import {
  BlockQuote,
  Cite,
  Deck,
  Heading,
  ListItem,
  List,
  Quote,
  Slide,
  Text,
  Image,
  Appear,
  Code,
  CodePane,
  Fill,
  Layout
} from "spectacle";

// Import theme
import createTheme from "spectacle/lib/themes/default";

// Require CSS
require("normalize.css");

const theme = createTheme({
  primary: "white",
  secondary: "#1F2022",
  tertiary: "#03A9FC",
  quarternary: "#CECECE"
}, {
  primary: "Montserrat",
  secondary: "Helvetica"
});

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck transition={["zoom", "slide"]} transitionDuration={500} theme={theme}>
        <Slide transition={["zoom"]} bgColor="primary">
          <Heading size={3} textColor="secondary">Backuping Postgree Databse on RDS</Heading>
          <a href="http://localhost:3000/#/amazon-rds">
            <Image src="https://www.percona.com/blog/wp-content/uploads/2016/07/Amazon-RDS-and-pt-online-schema-change.png" href="amazon-rds">
            </Image>
          </a>
          <a href="http://localhost:3000/#/postgree-sql">
            <Image src="http://www.savepoint.blog.br/wp-content/uploads/2015/01/postgresql-logo.png" href="postgree-sql">
            </Image>
          </a>
        </Slide>

        <Slide transition={['fade']} bgColor="tertiary" textColor="primary">
            <List>
              <Heading size={3} textColor="primary" caps>Quais os passos?</Heading>
              <Appear><ListItem>Dump da RDS para S3</ListItem></Appear>
              <Appear><ListItem>Restore</ListItem></Appear>
            </List>
          </Slide>

          <Slide transition={['zoom']} bgColor="primary">
           <Heading size={1} fit caps lineHeight={1} textColor={this.secondary}>
             Dump
           </Heading>
         </Slide>

         <Slide transition={['zoom']} bgColor="secondary">
           <Heading size={5}  textColor="primary" caps>Executar dentro de uma instância EC2</Heading>
           <CodePane bgColor="tertiary" source="$ sudo yum update && sudo yum -y install postgresql95" style="font-size:0.7em;">
           </CodePane>
        </Slide>

        <Slide transition={['zoom']} bgColor="secondary">
          <CodePane bgColor="tertiary" source="$ pg_dump -v -c -C -O -o -h sis-production.capetyhxzxr5.us-west-2.rds.amazonaws.com -U sisproduction sgc | aws s3 cp - s3://sis-production/dump-sis-201801300947.sql" style="font-size:0.7em;">
          </CodePane>
        </Slide>

        <Slide transition={["fade"]} bgColor="primary" textColor="tertiary">
          <Heading size={4} textColor="secondary" caps>Entendendo o comando</Heading>
          <List>
            <Heading size={6} textColor="secondary" caps>-v: Verbose</Heading>
            <ListItem>
              Saída detalhada do processo como comentários, início, término e mensagens de progresso</ListItem>

            <Heading size={6} textColor="secondary" caps>-c: Clean</Heading>
            <ListItem>
              Comandos de saída para limpar objetos de banco de dados antes de enviar os comandos para criá-los</ListItem>

            <Heading size={5} textColor="secondary" caps>-C: Create</Heading>
            <ListItem>
              Comando para criar o banco e reconectar ao banco criado
            </ListItem>
          </List>
        </Slide>

        <Slide transition={["fade"]} bgColor="primary" textColor="tertiary">
          <List>
            <Heading size={5} textColor="secondary" caps>-O: No Owner</Heading>
            <ListItem>
              Não roda comandos para determinar o owner do banco que são realizados por default
            </ListItem>

            <Heading size={6} textColor="secondary" caps>-o: oids</Heading>
            <ListItem>
              Executa o dump nos (OIDs) como parte do conteúdo da tabela
            </ListItem>

            <Heading size={6} textColor="secondary" caps>-h: Host</Heading>
            <ListItem>
              Especifica o host name da máquina em que o servidar está rodando
            </ListItem>
          </List>
        </Slide>

        <Slide transition={['zoom']} bgColor="primary">
         <Heading size={1} fit caps lineHeight={1} textColor={this.secondary}>
           Restore
         </Heading>
       </Slide>

       <Slide transition={['zoom']} bgColor="secondary">
         <Heading size={5}  textColor="primary" caps>Trocar a permissão do arquivo s3</Heading>
         <CodePane bgColor="tertiary" source="$ https://s3.console.aws.amazon.com/s3/buckets/sis-production/?region=us-west-2&tab=overview" style="font-size:0.7em;">
         </CodePane>
      </Slide>

      <Slide transition={['zoom']} bgColor="secondary">
        <CodePane bgColor="tertiary" source="$ more > make public" style="font-size:0.7em;">
        </CodePane>
     </Slide>

     <Slide transition={["fade"]} bgColor="primary" textColor="tertiary">
       <List>
         <Heading size={5} textColor="secondary" caps>Para restaurar em staging</Heading>
         <Heading size={6} textColor="secondary" caps>Remover a instância</Heading>
         <ListItem> Instance actions > delete </ListItem>
         <Heading size={6} textColor="secondary" caps>Criar a nova instância</Heading>
         <ListItem>Launch DB Instance</ListItem>
         <ListItem>Postgree SQL</ListItem>
         <ListItem>Dev/Test</ListItem>
         <ListItem>Copiar configurações da anterior</ListItem>
       </List>
     </Slide>

     <Slide transition={['zoom']} bgColor="secondary">
      <Heading size={5} textColor="primary" caps>Restaurar o DUMP na nova instância</Heading>
      <CodePane bgColor="tertiary" source="$ sudo yum update && sudo yum -y install postgresql95" style="font-size:0.7em;">
      </CodePane>
    </Slide>

    <Slide transition={['zoom']} bgColor="secondary">
     <CodePane bgColor="tertiary" source="$ curl https://s3-us-west-2.amazonaws.com/sis-production/dump-sis-201801300947.sql | psql -h sis-staging.capetyhxzxr5.us-west-2.rds.amazonaws.com -U sgc sgc" style="font-size:0.7em;">
     </CodePane>
   </Slide>

    <Slide transition={['zoom']} bgColor="primary">
     <Heading size={1} fit caps lineHeight={1} textColor={this.secondary}>
       Demo
     </Heading>
    </Slide>

    <Slide transition={['zoom', 'fade']} bgColor="primary">
      <Heading>PSQL</Heading>
      <Code bgColor="tertiary" textColor="primary" style="display:flex;flex-direction:row;align-items:center;justify-content:center;font-size:.8em;">psql -h sis-staging-new.capetyhxzxr5.us-west-2.rds.amazonaws.com -U sgc sgc  drop database sgc</Code>
      <Layout>
        <Fill>
          <Heading size={6} caps textColor="secondary" bgColor="white" margin={10}>
            <a href="http://localhost:3000/#/4">O que posso fazer no PSQL?</a>
          </Heading>
        </Fill>
      </Layout>
    </Slide>


        <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
          <BlockQuote>
            <Quote>Fim</Quote>
            <Cite>Ateliê de Software</Cite>
          </BlockQuote>
        </Slide>

        <Slide id="amazon-rds" transition={['fade']} bgColor="tertiary" textColor="primary">
            <List>
              <Heading size={3} textColor="primary" caps>Amazon RDS</Heading>
              <ListItem>O Amazon Relational Database Service (Amazon RDS) facilita configurar, operar e escalar bancos de dados relacionais na nuvem.</ListItem>
            </List>
          </Slide>

          <Slide id="postgree-sql" transition={['fade']} bgColor="tertiary" textColor="primary">
              <List>
                <Heading size={3} textColor="primary" caps>Postgree SQL</Heading>
                <ListItem>É uma ferramenta open-source para banco de dados relacionais.</ListItem>
              </List>
            </Slide>

      </Deck>
    );
  }
}
